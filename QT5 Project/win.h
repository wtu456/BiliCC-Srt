﻿#ifndef WIN_H
#define WIN_H

#include <QMainWindow>
#include <QtNetwork/QNetworkReply>
#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QJsonArray>
#include <QWinTaskbarButton>
#include <QWinTaskbarProgress>

QT_BEGIN_NAMESPACE
namespace Ui { class win; }
QT_END_NAMESPACE

class win : public QMainWindow
{
    Q_OBJECT

public:
    win(QWidget *parent = nullptr);
    ~win();
    QByteArray linShi;
    QByteArray muLuJsonByt;
    QJsonArray muLuArray;
    QString ming;
    QString biaoti;
    QString pming;
    QString bianma = "UTF-8";
    int jin;

private slots:
    void cao();
    void requestFinished(QNetworkReply* reply);
    void bianmaqiehuan();

private:
    Ui::win *ui;
    QNetworkAccessManager *naManager;   //网络模块控制器
    QWinTaskbarButton *renwulanbut;
    QWinTaskbarProgress *renwulanjindutiao;
    QEventLoop loop;                    //事件循环器
    QJsonArray getShiPinLeiBiao(QString avhao); //获取视频目录列表
    void xiaZaiDanPZiMu(QString aid,QString cid,int p);
    void getJsonZhuanSrt(QString url);
    QString miaoShuZhuanHuan(double miaoShu);
    QString qv(QString str);
    QByteArray zhuanbianma(QString str);
};
#endif // WIN_H
